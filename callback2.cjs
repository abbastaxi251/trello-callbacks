/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs")

const readBoardsListsFromFile = (filePath, callback) => {
    fs.readFile(filePath, "utf8", (err, boardsLists) => {
        if (err) {
            console.error("Error reading cards data:", err)
            callback(err)
        } else {
            // console.log(boardsLists)
            callback(null,boardsLists)
        }
    })
}

const getBoardListsById = (boardId, callback) => {
    readBoardsListsFromFile("lists.json", (err, boardsData) => {
        let boardLists = null
        if (err) {
            console.error("error reading lists.json", err)
            callback(null)
        } else {

            const jsonBoardsData  = JSON.parse(boardsData)
            for (let board in jsonBoardsData) {
                // console.log(jsonBoardsData[board])
                if (board === boardId) {
                    boardLists = jsonBoardsData[board]
                    break
                }
            }
            // console.log(boardInformation)
            callback(JSON.stringify(boardLists,null,2))
        }
    })
}

module.exports = getBoardListsById
