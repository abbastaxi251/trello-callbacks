const callback1 = require('../callback3.cjs')

const callbackFunction = (cardInfo) => {
    if (cardInfo) {
        console.log(cardInfo)
    } else {
        console.log("Card not found.")
    }
}

callback1("jwkh245",callbackFunction)
