const callback1 = require("../callback1.cjs")

const callbackFunction = (boardInfo) => {
    if (boardInfo) {
        console.log(boardInfo)
    } else {
        console.log("Board not found.")
    }
}

callback1("mcu453ed", callbackFunction)
