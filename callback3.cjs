/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs")

const readCardsFromFile = (filePath, callback) => {
    fs.readFile(filePath, "utf8", (err, cards) => {
        if (err) {
            console.error("Error reading cards data:", err)
            callback(err)
        } else {
            // console.log(cards)
            callback(null, cards)
        }
    })
}

const getCardsById = (boardId, callback) => {
    readCardsFromFile("cards.json", (err, cardsData) => {
        let cards = null
        if (err) {
            console.error("error reading lists.json", err)
            callback(null)
        } else {
            const jsonCardsData = JSON.parse(cardsData)
            for (let card in jsonCardsData) {
                // console.log(jsonCardsData[board])
                if (card === boardId) {
                    cards = jsonCardsData[card]
                    break
                }
            }
            // console.log(boardInformation)
            callback(JSON.stringify(cards, null, 2))
        }
    })
}

module.exports = getCardsById
