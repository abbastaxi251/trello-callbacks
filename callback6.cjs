/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const fs = require("fs")

const boardsDataFetcher = require("./callback1.cjs")
const listsDataFetcher = require("./callback2.cjs")
const cardsDataFetcher = require("./callback3.cjs")

const getInfo = (boardId) => {
    const Info = []
    boardsDataFetcher(boardId, (boardInfo) => {
        if (boardInfo) {
            // console.log(JSON.parse(boardInfo).id)
            Info.push(JSON.parse(boardInfo))
            listsDataFetcher(JSON.parse(boardInfo).id, (listsInfo) => {
                if (listsInfo) {
                    // console.log(boardInfo)
                    // console.log(listsInfo)
                    Info.push(JSON.parse(listsInfo))
                    // console.log(typeof listsInfo)
                    listsInfo = JSON.parse(listsInfo)
                    // console.log(typeof listsInfo)
                    // console.log(listsInfo)
                    for (let index = 0; index < listsInfo.length; index++) {
                        // console.log(listsInfo[index])
                        cardsDataFetcher(listsInfo[index].id, (cardInfo) => {
                            if (cardInfo) {
                                // console.log(cardInfo)
                                Info.push(JSON.parse(cardInfo))
                                if (index === listsInfo.length - 1) {
                                    console.log(Info)
                                }
                                // console.log(Info)
                            }
                        })
                    }
                }
            })
        }
    })
}

module.exports = getInfo
