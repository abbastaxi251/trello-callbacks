/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs")

const readBoardsFromFile = (filePath, callback) => {
    fs.readFile(filePath, "utf8", (err, boardsData) => {
        if (err) {
            console.error("Error reading boards data:", err)
            callback(err)
        } else {
            // console.log(boardsData)
            callback(null,boardsData)
        }
    })
}

const getBoardInfoById = (boardId, callback) => {
    readBoardsFromFile("boards.json", (err, boardsData) => {
        let boardInformation = null
        if (err) {
            console.error("error reading boards.json", err)
            callback(null)
        } else {

            const jsonBoardsData  = JSON.parse(boardsData)
            for (let board in jsonBoardsData) {
                // console.log(jsonBoardsData[board])
                if (jsonBoardsData[board].id === boardId) {
                    boardInformation = jsonBoardsData[board]
                    break
                }
            }
            // console.log(boardInformation)
            callback(JSON.stringify(boardInformation,null,2))
        }
    })
}

module.exports = getBoardInfoById
